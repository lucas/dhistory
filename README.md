# Updating trends.debian.net

Reserve and prepare a node on Grid'5000:
```
oarsub -q production -p "cluster='grvingt'" -l walltime=8 'sleep 1d'
oarsub -l walltime=8 'sleep 1d'
oarsub -C ...

sudo-g5k && \
sudo apt-get update && \
sudo apt-get -y install lintian/bullseye-backports && \
sudo dpkg -i lintian_2.115.3_all.deb && \
sudo apt-get -y install ruby-thor ruby-net-sftp ruby-net-scp ruby-peach
```

Note: connect using ssh -A to provide credentials to connect to debian.org machines

Populate the farm:
```
cd resist/lnussbaum/dhistory/
./dhistory fetch-snapshot-data

./dhistory populate-farm
```

Now follow instructions to fetch all required files

Do the analysis itself:
```
./dhistory analyze-all 10 # maybe run on several nodes
```

Generate results:
```
./dhistory collect-results

./dhistory fetch-udd-maintainers

./dhistory generate-html

./dhistory generate-graphs
```

# Disk space requirements

- farm/ (which contains the partial copy of snapshot.d.o) requires about 840 GB.

- cache/ takes about 7 GB.

- a farm/ for PREFIX=0 (enough for development) thus should take about 840/16 = 52 GB.


# Development

The easiest way to hack on trends.d.n is to use only a subset of the data.

After:
./dhistory fetch-snapshot-data

Do:
./dhistory filter-snapshot-for-checksums

And then set:
PREFIX=0 or PREFIX=00
to restrict all further commands to packages where md5sum(name) starts with 0 or 00.

## As a non-DD

While all the data that trends.d.n uses is public, accessing it in a convenient way requires being a DD. If you still want to hack on trends.d.n, you could ask a DD to run:

- fetch-snapshot-data
- filter-snapshot-json-for-checksums
- set PREFIX=0
- populate-farm
- fetch-udd-maintainers

And then provide you the resulting data.

# History
## April 2019

* Initial work

## July 2019

* Refreshed data
* Added data about DEP5 copyright format
* Added data about autopkgtest adoption
* Added dgit in VCS hosting graph
* Compute smells based on date (do not complain about stuff that did not exist yet)
* Use a more sensible ordering for datasets in all graphs
* Nicer colors in graphs
* Documentation / code cleanup

## September 2019

* Refreshed data
